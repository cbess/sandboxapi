
/**
Express 4.x

http://expressjs.com/4x/api.html
 */

var express = require('express'),
    routes = require('../routes'),
    cons = require('consolidate'),
    less = require('less'),
    fs = require('fs'),
    util = require('util'),
    path = require('path'),
    // The config is used for 'private' config options.
    config = require('../local.config');

var app = express();

// setup redis
var redis = null,
    redisClient = null;
if (config.redis_config) {
    redis = require('redis');
    var info = config.redis_config['redis-2.2'];
    if (info) {
        var connInfo = info['credentials'];
        // console.log(connInfo);
        redisClient = redis.createClient(connInfo.port, connInfo.host);
        if (connInfo.password) {
            redisClient.auth(connInfo.password);
        }
    }
}

// all environments
app.set('views', __dirname + '/views');
app.set('redis config', config.redis_config);
app.set('redis client', redisClient);
// set .html as the default extension 
app.set('view engine', 'ejs');

app.use(function(req, res, next) {
    req.redisClient = redisClient;
    next();
});

// app.use(express.favicon());
// app.use(express.logger('dev'));
app.use(require('body-parser').json());
app.use(require('method-override')());
// app.use(express.session());
app.use(require('stylus').middleware(__dirname + '/public'));
app.use(express.static(path.join(__dirname, 'public')));

// assign the template engine to files
app.engine('hbs', cons.handlebars);
app.engine('ejs', cons.ejs);

if ('development' == app.get('env')) {
    /*
    app.use(function(err, req, res, next) {
        res.send(config.redis_config);
        next(err);
    });
    */
    app.use(require('errorhandler')());
    
    // compile `less` to `css`, then send it as the response
    app.get('*.less', function(req, res) {
        var path = util.format('%s/public%s', __dirname, req.url);
        fs.readFile(path, 'utf8', function(err, data) {
            if (err) throw err;
            less.render(data, function(err, css) {
                if (err) throw err;
                res.header('Content-type', 'text/css');
                res.send(css);
            });
        });
    });
}

// GET
app.get('/', routes.index);
// app.get('/redis-index', routes.redisTestIndex);
// ANY
// app.all('/add-more', routes.addMore);

exports.app = app;

