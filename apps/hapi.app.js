// hapi
// http://hapijs.com/tutorials

var hapi = require('hapi');

exports.hapi = hapi;
var createServerCallback = null;

// allow the user to setup the server outside of this module
exports.createServer = function(cb) {
	createServerCallback = cb;
};

// run the server, after it has been setup
exports.run = function() {
	var server = createServerCallback(hapi);
	server.start(function () {
	    console.log('hapi server running at:', server.info.uri);
	});

	server.route([
	  {
	    path: '/api/',
	    method: 'GET',
	    config: {
	      handler: function(req, reply) {
	        reply({
	        	title: 'hapi: a great title',
	        	itemId: 77
	        });
	      }
	    }
	  },
	  {
	    path: '/api/items',
	    method: 'PUT',
	    config: {
	      handler: function(req, reply) {
	      	reply('ok');
	        // req.item.set(req.body);
	        // req.item.save(function(err, item) {
	        //   reply(item).code(204);
	        // });
	      }
	    }
	 }]);
};
