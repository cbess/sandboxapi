var _ = require('underscore');
// fake global database object
var gData = {};

exports.index = function(req, res) {
    var context = { 
        title: 'Demo', 
        body: 'express: body text',
        count: 7
    };
    
    // now render index
    res.send(context);
};